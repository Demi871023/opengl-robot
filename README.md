# OpenGL Robot

## Directory Structure

```
.
├── README.md
├── Demo
│   ├─ obj          the 3D model for robot each component
│   ├─ texture      the .png or .jpg for texture source
│   ├─ .
│   ├─ .
│   └─ main.exe
├── demo.gif
└── **main.cpp**   
``` 

## About the project
This is an OpenGL robotics project written in C++. In the **main.cpp** file, I import several models to build my robot and enable its movement, rotate and dance in a 3D space through code. Users can interact using the mouse and the keyboard. The detailed methods of interaction are as follows.

- movement(up, down, left and right): wsad
- rotate(left and right): qe
- movement(near and far): mouse wheel
- dance: press the right button of mouse to open menu, then click the start!

### Notice
You can execute Demo/main.exe to try it!

## Demo
![image](https://gitlab.com/Demi871023/opengl-robot/-/raw/main/demo.gif)