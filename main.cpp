#include "../Include/Common.h"

//For GLUT to handle 
#define MENU_TIMER_START 1
#define MENU_TIMER_STOP 2
#define MENU_EXIT 3
#define MENU_HELLO 4
#define MENU_WORLD 5

//GLUT timer variable
float timer_cnt = 0;
bool timer_enabled = false;
bool _armenabled = true;
bool _legenabled = true;
bool _headenabled = true;
unsigned int timer_speed = 16;

using namespace glm;
using namespace std;

mat4 view(1.0f);			// V of MVP, viewing matrix
mat4 projection(1.0f);		// P of MVP, projection matrix
mat4 model(1.0f);			// M of MVP, model matrix
vec3 temp = vec3();			// a 3 dimension vector which represents how far did the ladybug should move

GLint um4p;
GLint um4mv;

GLuint program;			// shader program id

typedef struct
{
	GLuint vao;			// vertex array object
	GLuint vbo;			// vertex buffer object

	int materialId;
	int vertexCount;
	GLuint m_texture;
} Shape;

vector <string> m_list{ "Capsule.obj", "Cube.obj", "Cylinder.obj", "Sphere.obj", "Plane.obj" };
vector <Shape> m_shape(m_list.size());
vector <mat4> M(10);
vector <float> pos({ 0.0f, 0.0f, 0.0f});

map <int, vec3> _robotmrotate = {	{0, vec3(0.0f, 1.0f, 0.0f),},
									{1, vec3(1.0f, 0.0f, 0.0f),},
									{2, vec3(1.0f, 0.0f, 0.0f),},
									{3, vec3(1.0f, 0.0f, 0.0f),},
									{4, vec3(1.0f, 0.0f, 0.0f),},
									{5, vec3(1.0f, 0.0f, 0.0f),},
									{6, vec3(1.0f, 0.0f, 1.0f),},
									{7, vec3(0.0f, 0.0f, 1.0f),},
									{8, vec3(1.0f, 0.0f, 1.0f),},
									{9, vec3(0.0f, 0.0f, 1.0f),},
};

float _robotdegree[10] = { 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };



map<string, int> _robotidx = { {"Head", 0, },
								{"Torso", 1, },
								{"L1_Arm", 2, },
								{"L2_Arm", 3, },
								{"R1_Arm", 4, },
								{"R2_Arm", 5, },
								{"L1_Leg", 6, },
								{"L2_Leg", 7, },
								{"R1_Leg", 8, },
								{"R2_Leg", 9, },
};
map<string, string> _robotmshpae{ {"Head", "Cube",},
								{"Torso", "Cube",},
								{"L1_Arm", "Cylinder", },
								{"L2_Arm", "Capsule", },
								{"R1_Arm","Cylinder", },
								{"R2_Arm", "Capsule", },
								{"L1_Leg", "Cylinder", },
								{"L2_Leg", "Capsule", },
								{"R1_Leg", "Cylinder", },
								{"R2_Leg", "Capsule", },
};

map<string, int> _msahpeidx = { {"Capsule", 0, },		// 膠囊體
								{"Cube", 1, },			// 正方體
								{"Cylinder", 2, },		// 圓柱體
								{"Sphere", 3, },		// 球體
								{"Plane", 4, },			// 平面
};


// Load shader file to program
char** loadShaderSource(const char* file)
{
	FILE* fp = fopen(file, "rb");
	fseek(fp, 0, SEEK_END);
	long sz = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	char *src = new char[sz + 1];
	fread(src, sizeof(char), sz, fp);
	src[sz] = '\0';
	char **srcp = new char*[1];
	srcp[0] = src;
	return srcp;
}

// Free shader file
void freeShaderSource(char** srcp)
{
	delete srcp[0];
	delete srcp;
}

// Load .obj model
void My_LoadModels(int mid, char filename[], char texture[])
{
	// init
	m_shape[mid].vao = 0;
	m_shape[mid].vbo = 0;

	tinyobj::attrib_t attrib;
	vector<tinyobj::shape_t> shapes;
	vector<tinyobj::material_t> materials;
	string warn;
	string err;

	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, filename);

	if (!warn.empty()) {
		cout << warn << endl;
	}
	if (!err.empty()) {
		cout << err << endl;
	}
	if (!ret) {
		exit(1);
	}

	vector<float> vertices, texcoords, normals;  // if OBJ preserves vertex order, you can use element array buffer for memory efficiency
	for (int s = 0; s < shapes.size(); ++s) {  // for 'ladybug.obj', there is only one object
		int index_offset = 0;
		for (int f = 0; f < shapes[s].mesh.num_face_vertices.size(); ++f) {
			int fv = shapes[s].mesh.num_face_vertices[f];
			for (int v = 0; v < fv; ++v) {
				tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
				vertices.push_back(attrib.vertices[3 * idx.vertex_index + 0]);
				vertices.push_back(attrib.vertices[3 * idx.vertex_index + 1]);
				vertices.push_back(attrib.vertices[3 * idx.vertex_index + 2]);
				texcoords.push_back(attrib.texcoords[2 * idx.texcoord_index + 0]);
				texcoords.push_back(attrib.texcoords[2 * idx.texcoord_index + 1]);
				normals.push_back(attrib.normals[3 * idx.normal_index + 0]);
				normals.push_back(attrib.normals[3 * idx.normal_index + 1]);
				normals.push_back(attrib.normals[3 * idx.normal_index + 2]);
			}
			index_offset += fv;
			m_shape[mid].vertexCount += fv;
		}
	}

	glGenVertexArrays(1, &m_shape[mid].vao);
	glBindVertexArray(m_shape[mid].vao);

	glGenBuffers(1, &m_shape[mid].vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m_shape[mid].vbo);

	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float) + texcoords.size() * sizeof(float) + normals.size() * sizeof(float), NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof(float), vertices.data());
	glBufferSubData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), texcoords.size() * sizeof(float), texcoords.data());
	glBufferSubData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float) + texcoords.size() * sizeof(float), normals.size() * sizeof(float), normals.data());

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)(vertices.size() * sizeof(float)));
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)(vertices.size() * sizeof(float) + texcoords.size() * sizeof(float)));
	glEnableVertexAttribArray(2);

	shapes.clear();
	shapes.shrink_to_fit();
	materials.clear();
	materials.shrink_to_fit();
	vertices.clear();
	vertices.shrink_to_fit();
	texcoords.clear();
	texcoords.shrink_to_fit();
	normals.clear();
	normals.shrink_to_fit();

	cout << "Load " << m_shape[mid].vertexCount << " vertices" << endl;

	texture_data tdata = loadImg(texture);

	glGenTextures(1, &m_shape[mid].m_texture);
	glBindTexture(GL_TEXTURE_2D, m_shape[mid].m_texture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, tdata.width, tdata.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, tdata.data);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	delete tdata.data;
}

// OpenGL initialization
void My_Init()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	// Create Shader Program
	program = glCreateProgram();

	// Create customize shader by tell openGL specify shader type 
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	// Load shader file
	char** vertexShaderSource = loadShaderSource("vertex.vs.glsl");
	char** fragmentShaderSource = loadShaderSource("fragment.fs.glsl");

	// Assign content of these shader files to those shaders we created before
	glShaderSource(vertexShader, 1, vertexShaderSource, NULL);
	glShaderSource(fragmentShader, 1, fragmentShaderSource, NULL);

	// Free the shader file string(won't be used any more)
	freeShaderSource(vertexShaderSource);
	freeShaderSource(fragmentShaderSource);

	// Compile these shaders
	glCompileShader(vertexShader);
	glCompileShader(fragmentShader);

	// Logging
	shaderLog(vertexShader);
	shaderLog(fragmentShader);

	// Assign the program we created before with these shaders
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);

	// Get the id of inner variable 'um4p' and 'um4mv' in shader programs
	um4p = glGetUniformLocation(program, "um4p");
	um4mv = glGetUniformLocation(program, "um4mv");

	// Tell OpenGL to use this shader program now
	glUseProgram(program);

	My_LoadModels(0, "obj/Capsule.obj", "texture/grunge-wall-texture.jpg");	// 膠囊體
	My_LoadModels(1, "obj/Cube.obj", "texture/gold_texture.jpg");		// 正方體
	My_LoadModels(2, "obj/Cylinder.obj", "texture/grunge-wall-texture.jpg");	// 圓柱體
	My_LoadModels(3, "obj/Sphere.obj", "texture/skin.png");		// 球體
	My_LoadModels(4, "obj/Plane.obj", "texture/body.png");		// 平面
}

void My_CalcuModel(string part, mat4 trmodel, mat4 smodel)
{
	if (part == "Head")						// 0 Head
		model = M[1] * trmodel * smodel;
	if (part == "Torso")					// 1 Torso
		model = trmodel * smodel;
	if (part == "L1_Arm")					// 2 L1_Arm
		model = M[1] * trmodel * smodel;
	if (part == "L2_Arm")					// 3 L2_Arm
		model = M[1] * M[2] * trmodel * smodel;
	if (part == "R1_Arm")					// 4 R1_Arm
		model = M[1] * trmodel * smodel;
	if (part == "R2_Arm")					// 5 R2_Arm
		model = M[1] * M[4] * trmodel * smodel;
	if (part == "L1_Leg")					// 6 L1_Leg
		model = M[1] * trmodel * smodel;
	if (part == "L2_Leg")					// 7 L2_Leg
		model = M[1] * M[6] * trmodel * smodel;
	if (part == "R1_Leg")					// 8 R1_Leg
		model = M[1] * trmodel * smodel;
	if (part == "R2_Leg")					// 9 R2_Leg
		model = M[1] * M[8] * trmodel * smodel;

}

// part : 部位名稱, t : 位移, degree : 旋轉角度 , r : 旋轉方向 , s : 縮放
void My_DrawRobot(string part, vec3 t, float degree, vec3 r, vec3 s)
{
	// 取得身體部位 id
	int _pid = _robotidx.find(part)->second;
	// 取得身體部位所使用的模型 name
	string _mname = _robotmshpae.find(part)->second;
	// 利用模型 name 取得 id
	int _mid = _msahpeidx.find(_mname)->second;

	// 使用該模型座標資訊 (vao) 以及材質 (texture)
	glBindVertexArray(m_shape[_mid].vao);
	glUseProgram(program);
	glBindTexture(GL_TEXTURE_2D, m_shape[_mid].m_texture);

	mat4 _translate = translate(mat4(1.0f), t);		// 平移 vec3 位置
	mat4 _rotate = rotate(mat4(1.0f), radians(degree), r);	// 沿著 vec3 軸方向旋轉 radians 度
	mat4 _scale = scale(mat4(1.0f), s);							// 縮放 vec3 大小
	mat4 _tmpmodel = _translate * _rotate;
	//mat4 _tmpmodel = _translate * _scale;
	M[_pid] = _tmpmodel;
	

	My_CalcuModel(part, _tmpmodel, _scale);

	// 畫出模型
	glUniformMatrix4fv(um4mv, 1, GL_FALSE, value_ptr(view * model));
	glUniformMatrix4fv(um4p, 1, GL_FALSE, value_ptr(projection));
	glDrawArrays(GL_TRIANGLES, 0, m_shape[_mid].vertexCount);
}


// GLUT callback. Called to draw the scene.
void My_Display()
{
	// Clear display buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	

	float _robotposDisplay = pos[2];
	float _robotposVertical = pos[0];
	float _robotposHorizontal = pos[1];

	


	//My_DrawRobot("Torso", vec3(_robotposDisplay, _robotposHorizontal, _robotposVertical), timer_cnt, vec3(0.0, 1.0, 0.0), vec3(1.0, 3.0, 3.5));
	My_DrawRobot("Torso", vec3(_robotposDisplay, _robotposHorizontal, _robotposVertical), timer_cnt, vec3(0.0, 1.0, 0.0), vec3(1.0, 3.0, 3.5));
	My_DrawRobot("Head", vec3(0.0, 2.0, 0.0), _robotdegree[0], _robotmrotate[1], vec3(1.0, 1.0, 1.5));
	My_DrawRobot("L1_Arm", vec3(0.0, 0.5, -2.0), _robotdegree[2], _robotmrotate[2], vec3(1.0 , 1.0, 1.0));
	My_DrawRobot("R1_Arm", vec3(0.0, 0.5, 2.0), _robotdegree[4], _robotmrotate[4], vec3(1.0, 1.0, 1.0));
	My_DrawRobot("L2_Arm", vec3(0.0, -2.0, 0.0), _robotdegree[3], _robotmrotate[3], vec3(1.0, 1.0, 1.0));
	My_DrawRobot("R2_Arm", vec3(0.0, -2.0, 0.0), _robotdegree[5], _robotmrotate[5], vec3(1.0, 1.0, 1.0));
	My_DrawRobot("L1_Leg", vec3(0.0, -2.0, -1.0), _robotdegree[6], _robotmrotate[6], vec3(1.0, 1.0, 1.0));
	My_DrawRobot("R1_Leg", vec3(0.0, -2.0, 1.0), _robotdegree[8], _robotmrotate[8], vec3(1.0, 1.0, 1.0));
	My_DrawRobot("L2_Leg", vec3(0.0, -2.0, 0.0), _robotdegree[7], _robotmrotate[7], vec3(1.0, 1.0, 1.0));
	My_DrawRobot("R2_Leg", vec3(0.0, -2.0, 0.0), _robotdegree[9], _robotmrotate[9], vec3(1.0, 1.0, 1.0));
	
	
	glutSwapBuffers();
}

// Setting up viewing matrix
void My_Reshape(int width, int height)
{
	glViewport(0, 0, width, height);

	float viewportAspect = (float)width / (float)height;

	// perspective(fov, aspect_ratio, near_plane_distance, far_plane_distance)
	// ps. fov = field of view, it represent how much range(degree) is this camera could see 
	projection = perspective(radians(60.0f), viewportAspect, 0.1f, 1000.0f);

	// lookAt(camera_position, camera_viewing_vector, up_vector)
	// up_vector represent the vector which define the direction of 'up'
	view = lookAt(vec3(-10.0f, 5.0f, 0.0f), vec3(1.0f, 1.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));
}

void My_Timer(int val)
{
	if (timer_enabled)
	{
		timer_cnt += 1.0f;
		
		if (_armenabled == true)
		{
			if(_robotdegree[2] > 120 && _robotdegree[4] < -120)
				_armenabled = false;
			_robotdegree[2] = _robotdegree[2] + 2.0f;
			_robotdegree[3] = _robotdegree[3] + 0.25f;
			_robotdegree[4] = _robotdegree[4] - 2.0f;
			_robotdegree[5] = _robotdegree[5] - 0.25f;
		}
		else if(_armenabled == false)
		{
			if(_robotdegree[2] < 0 && _robotdegree[4] > 0)
				_armenabled = true;
			_robotdegree[2] = _robotdegree[2] - 2.0f;
			_robotdegree[3] = _robotdegree[3] - 0.25f;
			_robotdegree[4] = _robotdegree[4] + 2.0f;
			_robotdegree[5] = _robotdegree[5] + 0.25f;
		}

		if (_legenabled == true)
		{
			if (_robotdegree[6] > 15)
				_legenabled = false;
			_robotdegree[6] = _robotdegree[6] + 2.0f;
			_robotdegree[7] = _robotdegree[7] - 2.0f;
			_robotdegree[8] = _robotdegree[8] + 2.0f;
			_robotdegree[9] = _robotdegree[9] - 2.0f;
		}
		else if(_legenabled == false)
		{
			if (_robotdegree[6] < -15)
				_legenabled = true;
			_robotdegree[6] = _robotdegree[6] - 2.0f;
			_robotdegree[7] = _robotdegree[7] + 2.0f;
			_robotdegree[8] = _robotdegree[8] - 2.0f;
			_robotdegree[9] = _robotdegree[9] + 2.0f;
		}


		if (_headenabled == true)
		{
			if (_robotdegree[0] > 5)
				_headenabled = false;
			_robotdegree[0] = _robotdegree[0] + 0.5f;
		}
		else if (_headenabled == false)
		{
			if (_robotdegree[0] < -5)
				_headenabled = true;
			_robotdegree[0] = _robotdegree[0] - 0.5f;
		}

		glutPostRedisplay();		// 標記當前視窗需要重新繪製
		glutTimerFunc(timer_speed, My_Timer, val);
	}

	
}

void My_Keyboard(unsigned char key, int x, int y)
{
	// x, y 為鼠標所在位置
	printf("Key %c is pressed at (%d, %d)\n", key, x, y);
	if (key == 'd')
		pos[0] = pos[0] + 1;
	else if (key == 'a')
		pos[0] = pos[0] - 1;
	else if (key == 'w')
		pos[1] = pos[1] + 1;
	else if (key == 's')
		pos[1] = pos[1] - 1;
	else if (key == 'q')
		timer_cnt = timer_cnt - 1.0f;
	else if (key == 'e')
		timer_cnt = timer_cnt + 1.0f;

	glutPostRedisplay();
}

void My_SpecialKeys(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_F1:
		printf("F1 is pressed at (%d, %d)\n", x, y);
		break;
	case GLUT_KEY_PAGE_UP:
		printf("Page up is pressed at (%d, %d)\n", x, y);
		break;
	case GLUT_KEY_LEFT:
		printf("Left arrow is pressed at (%d, %d)\n", x, y);
		break;
	default:
		printf("Other special key is pressed at (%d, %d)\n", x, y);
		break;
	}
}

void My_Menu(int id)
{
	switch (id)
	{
	case MENU_TIMER_START:
		if (!timer_enabled)
		{
			timer_enabled = true;
			glutTimerFunc(timer_speed, My_Timer, 0);
		}
		break;
	case MENU_TIMER_STOP:
		timer_enabled = false;
		break;
	case MENU_EXIT:
		exit(0);
		break;
	case MENU_HELLO:
		// do something
		printf("Hello\n");
		break;
	case MENU_WORLD:
		// do something
		printf("World\n");
		break;
	default:
		break;
	}
}

void My_Mouse(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON)
	{
		if (state == GLUT_DOWN)
		{
			printf("Mouse %d is pressed at (%d, %d)\n", button, x, y);
		}
		else if (state == GLUT_UP)
		{
			printf("Mouse %d is released at (%d, %d)\n", button, x, y);
		}
	}
	// 滑鼠滾輪 上
	if (button == 3)
		pos[2] = pos[2] + 1;
	if (button == 4)
		pos[2] = pos[2] - 1;

	glutPostRedisplay();		// 標記當前視窗需要重新繪製
}

void My_newMenu(int id)
{

}

int main(int argc, char *argv[])
{
#ifdef __APPLE__
	// Change working directory to source code path
	chdir(__FILEPATH__("/../Assets/"));
#endif
	// Initialize GLUT and GLEW, then create a window.
	glutInit(&argc, argv);
#ifdef _MSC_VER
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
#else
	glutInitDisplayMode(GLUT_3_2_CORE_PROFILE | GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
#endif
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(600, 600);
	glutCreateWindow("Practice"); // You cannot use OpenGL functions before this line;
								  // The OpenGL context must be created first by glutCreateWindow()!
#ifdef _MSC_VER
	glewInit();
#endif
	dumpInfo();
	My_Init();

	int menu_main = glutCreateMenu(My_Menu);
	int menu_timer = glutCreateMenu(My_Menu);
	int menu_new = glutCreateMenu(My_Menu);

	glutSetMenu(menu_main);
	glutAddSubMenu("Timer", menu_timer);
	glutAddSubMenu("New", menu_new);
	glutAddMenuEntry("Exit", MENU_EXIT);

	glutSetMenu(menu_timer);
	glutAddMenuEntry("Start", MENU_TIMER_START);
	glutAddMenuEntry("Stop", MENU_TIMER_STOP);

	glutSetMenu(menu_new);						// Tell GLUT to design the menu which id==menu_new now
	glutAddMenuEntry("Hello", MENU_HELLO);		// Add submenu "Hello" in "New"(a menu which index is equal to menu_new)
	glutAddMenuEntry("World", MENU_WORLD);		// Add submenu "Hello" in "New"(a menu which index is equal to menu_new)

	glutSetMenu(menu_main);
	glutAttachMenu(GLUT_RIGHT_BUTTON);

	// Register GLUT callback functions.
	glutDisplayFunc(My_Display);
	glutReshapeFunc(My_Reshape);
	glutKeyboardFunc(My_Keyboard);
	glutSpecialFunc(My_SpecialKeys);
	glutTimerFunc(timer_speed, My_Timer, 0);
	// Todo
	// Practice 1 : Register new GLUT event listner here
	// ex. glutXXXXX(my_Func);
	// Remind : you have to implement my_Func
	glutMouseFunc(My_Mouse);

	// Enter main event loop.
	glutMainLoop();

	return 0;
}